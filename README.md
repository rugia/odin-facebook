This is the final rails project of The Odin Project.
This app has several basic facebook-like functions, such as user sign up/in, sending friend request, create posts, like and comment on posts.

Uploaded on Heroku:
[https://rugia-odin-facebook.herokuapp.com/](https://rugia-odin-facebook.herokuapp.com/)