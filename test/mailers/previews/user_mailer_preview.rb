# Preview all emails at http://top-facebook-rugia.c9users.io/rails/mailers/user_mailer
class UserMailerPreview < ActionMailer::Preview

    def welcome 
        UserMailer.welcome(User.first)
    end 
end
