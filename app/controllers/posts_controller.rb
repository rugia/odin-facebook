class PostsController < ApplicationController
  before_action :authenticate_user!
  
  def index
    @post = current_user.posts.new
    @posts = Post.where(author_id: current_user.id)
    current_user.friends.each do |f|
     Post.where(author_id: f.id).map{ |d| @posts << d }
    end
    @posts.order(created_at: :desc)
    
  end

  def new
    @user = current_user
    @post = @user.posts.new
  end
  
  def create
    @user = current_user
    @post = @user.posts.build(post_params).save
    redirect_to params[:post][:url]
  end

  def edit
    @post = Post.find(params[:id])
  end
  
  def update
    @post = Post.update(post_params)
    redirect_to params[:url]
  end
  
  def destroy
    @post = Post.find(params[:id])
    @post.destroy
    redirect_to user_path(current_user)
  end
  
  def like
    if UserPost.find_by(liked_post_id: Post.find(params[:id]), liking_user_id: current_user.id).nil?
      current_user.liked_posts << Post.find(params[:id]) 
      redirect_to params[:url]
    else
      redirect_to params[:url], alert: 'you already liked this post!'
    end
  end
  
  def unlike
    @post = Post.find(params[:id])
    @user_post = UserPost.find_by(liked_post_id: @post, liking_user_id: current_user.id)
    if @user_post
      @user_post.destroy
      redirect_to params[:url]
    else
      redirect_to params[:url], alert: 'you havent liked this post!'
    end
  end
  
  private
  
  def post_params
    params.require(:post).permit(:content, :picture)
  end
  
end
