class FriendablesController < ApplicationController
    
    def friend_request
        Friendable.create(from_id: current_user.id, to_id: params[:id], accepted: false)
        redirect_to params[:url]
    end
    
    def friend_request_accept
        friendable = Friendable.find_by(from_id: params[:id], to_id: current_user.id)
        friendable.update_attributes(accepted: true)
        redirect_to params[:url]
    end
    
    def friend_request_reject
        friendable = Friendable.find_by(from_id: params[:id], to_id: current_user.id)
        friendable.destroy
        redirect_to params[:url]
    end
end
