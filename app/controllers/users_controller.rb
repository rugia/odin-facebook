class UsersController < ApplicationController
    
    before_action :authenticate_user!
    
    def index
        @users = User.all
    end
    
    def show
        @user = User.find(params[:id])
        @post = @user.posts.new
        @posts = @user.posts.all.order(created_at: :desc)
    end
    
end
