class StaticPagesController < ApplicationController
  def index
    if user_signed_in?
      redirect_to posts_path
    end
  end
  
  def notification
    @notifications = Friendable.where(to_id: current_user.id, accepted: false)
  end
end
