class UserMailer < ApplicationMailer
    default from: 'rugia@theodinfacebook.com.tw'
    
    
    def welcome(user)
        @user = User.find(user.id)
        mail(to: @user.email, subject: 'Welcome to my site!')
    end
end
