class Post < ActiveRecord::Base
    belongs_to :author, class_name: 'User'
    has_many :comments
    has_many :user_posts, foreign_key: :liked_post_id
    has_many :liking_users, through: :user_posts
    mount_uploader :picture, PictureUploader
    validate :picture_size
    
    
    def picture_size
      if picture.size > 5.megabytes
        errors.add(:picture, "should be less than 5MB")
      end
    end
end
