class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  include Gravtastic
  gravtastic
  mount_uploader :image, PictureUploader
  validate :picture_size

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, 
         :omniauthable, :omniauth_providers => [:facebook]
         
  has_many :posts, foreign_key: :author_id
  has_many :comments
  
  # associations for 'Like' posts
  has_many :user_posts, foreign_key: :liking_user_id
  has_many :liked_posts, through: :user_posts
  
  # associations for friend invites
  has_many :friendables_from, foreign_key: :from_id, class_name: 'Friendable'
  has_many :friendables_to, foreign_key: :to_id, class_name: 'Friendable'
  has_many :inviters, through: :friendables_to, class_name: 'User'
  has_many :invited_users, through: :friendables_from, class_name: 'User'
  
  def friend_request_status(user)
    if Friendable.find_by(from_id: self.id, to_id: user.id, accepted: true) || Friendable.find_by(from_id: user.id, to_id: self.id, accepted: true)
      return 'friends'
    elsif Friendable.find_by(from_id: self.id, to_id: user.id, accepted: false) 
      return 'pending'
    elsif Friendable.find_by(from_id: user.id, to_id: self.id, accepted: false)
      return 'awaiting reply'
    else
      return nil
    end
  end
  
  def friends
    friends = []
    friendables = []
    friendables << Friendable.where(accepted: true).where(to_id: self.id)
    friendables << Friendable.where(accepted: true).where(from_id: self.id)
    friendables.each do |d|
      d.each do |f|
        if f.to_id == self.id
          puts'from_id'
          friends << User.find(f.from_id)
        else
          puts'to_id'
          friends << User.find(f.to_id)
        end
      end
    end
    friends
  end
  
  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
      user.email = auth.info.email
      user.password = Devise.friendly_token[0,20]
      user.name = auth.info.name   # assuming the user model has a name
      user.image = auth.info.image # assuming the user model has an image
    end
  end
  def picture_size 
    if image.size > 5.megabytes 
      errors.add(:image, "should be less than 5MB") 
    end 
  end 
end
