class Friendable < ActiveRecord::Base
    belongs_to :inviter, class_name: 'User', foreign_key: :from_id
    belongs_to :invited_user, class_name: 'User', foreign_key: :to_id
end
