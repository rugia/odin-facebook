# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
User.create(email: 'rugia813@yahoo.com.tw', name: 'Rugia', password: '88879888')
25.times do |t|
    User.create(email: Faker::Internet.email, name: Faker::Superhero.name, password: '88879888', image: Faker::Avatar.image)
    User.find(t+1).posts.create(content: Faker::Lorem.paragraph)
    Friendable.create(from_id: 1, to_id: t+2, accepted: true)
end