class CreateUserPosts < ActiveRecord::Migration
  def change
    create_table :user_posts do |t|
      t.integer :liked_post_id
      t.integer :liking_user_id

      t.timestamps null: false
    end
  end
end
