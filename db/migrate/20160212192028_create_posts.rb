class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.references :author
      t.string :content

      t.timestamps null: false
    end
  end
end
